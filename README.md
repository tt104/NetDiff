# NetDiff

## Installation

To install NetDiff, install the devtools R package and run the commands

    library(devtools)
    install_git("https://gitlab.com/tt104/NetDiff.git")

## Usage

Running the method, most parameter values can be left at their default values. A matrix containing columns for each gene, and one row per observation is supplied. The data should first be scaled to have column means of zero and variance one. A partition of the rows (observations) into two classes should be provided, and these can be named as desired.

    library(NetDiff)
    D<-scale(D)
    results<-netDiff(D,c(rep("control",10),rep("case",10)))

The results take the form of a list containing estimated partial correlations for each class, corresponding local false discovery rates and Bayes factors for each gene. The most straightforward way to inspect the results is to convert them into an iGraph object. Network nodes are named based on the column names of the data, so when using netDiffiGraph, column names for each gene must be provided in the matrix passed to the netDiff function.

    g<-netDiffiGraph(results)

The edges in the iGraph object are labelled with the classes used in the partition passed to the netDiff function as the attribute 'pheno'. Edges inferred in both classes are labelled as "Class 1, Class 2", e.g. "control, case". The iGraph object can be saved as a GraphML file for analysis in Cytoscape.

    write.graph(g,"graph.graphml",format="graphml")

To speed up inference the method can be run in parallel on a multicore CPU using the doParallel library.

    library(doParallel)
    registerDoParallel(cores=4)
    results<-netDiff(D,c(rep("control",10),rep("case",10)),parallel=TRUE)

## Issues

The method is conservative in detecting changes and it may be the case that the method is unable to detect any significant changes between the two sets of observations. In testing this was often found to occur with small sample sizes relative to the number of genes considered. 

The method cannot utilise data with missing (NA) values, and columns containing these must be removed before calling the netDiff function. Data containing identical columns may cause errors, this is generally unlikely but can occur if all observations for two or more genes are zero after centering. As these observations are uninformative any such columns should be removed.
