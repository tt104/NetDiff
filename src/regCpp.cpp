#include <RcppArmadillo.h>

// [[Rcpp::depends(RcppArmadillo)]]

using namespace Rcpp;

// [[Rcpp::export]]
List varUpdate(int nsteps, int j, NumericMatrix Dr, double alpha, double gamma, double salpha, double sbeta, NumericVector modelr, int K, int calc_ev)
{
	arma::mat D = Rcpp::as<arma::mat>(Dr);
	arma::colvec model = Rcpp::as<arma::colvec>(modelr);
	int W = D.n_cols;
	int M = D.n_cols-1;
	int L = D.n_rows;

	//variational parameters
	std::vector<arma::mat> Sigmas(K);
	std::vector<arma::colvec> mu(K);
	std::vector<arma::colvec> iz(K);
	for(int k=0;k<K;k++)
	{
		Sigmas[k] = arma::eye(M,M);
		mu[k] = arma::randn(M);
		//iz[k] = 50+100*arma::randu(M);
		iz[k] = arma::randu(M);
	}

	double err=4.0;
	double errA=salpha+L/2.0;
	double errB;

	//data
	arma::colvec Ytmp;
	arma::mat Xtmp;
	Ytmp = D.col(j);
	if(j>0)
	{
		if(j<(W-1))
		{
			Xtmp = arma::join_rows(D.cols(0,j-1),D.cols(j+1,W-1));
		}
		else
		{
			Xtmp = D.cols(0,W-2);
		}
	}
	else
	{
		Xtmp = D.cols(1,W-1);
	}
	//for each set of rows
	std::vector<arma::colvec> Y(K);
	std::vector<arma::mat> X(K);
	std::vector<int> Ls(K);
	for(int k=0;k<K;k++)
	{
		std::vector<unsigned int> indices;
		for(int j=0;j<L;j++)
		{
			if((model[j]-1)==k)
			{
				indices.push_back(j);
			}
		}
		arma::uvec aix(indices);
		X[k]=Xtmp.rows(aix);
		Y[k]=Ytmp.elem(aix);
		Ls[k]=indices.size();
	}

	//variational update loop
	for(int step=0;step<nsteps;step++)
	{
		//update regression
		double s = 0.0;
		for(int k=0;k<K;k++)
		{
			//update Sigma for k
			Sigmas[k] = inv(err*trans(X[k])*X[k]+diagmat(iz[k]));
			//mu
			mu[k] = err*Sigmas[k]*trans(X[k])*Y[k];

			//zeta
			double a = gamma*gamma;
			arma::colvec b = alpha*alpha + mu[k]%mu[k] + Sigmas[k].diag();
			for(int m=0;m<M;m++)
			{
				double sab = sqrt(a*b(m));
				iz[k](m) = pow(sqrt(b(m)/a),-1)*R::bessel_k(sab,2,1)/R::bessel_k(sab,1,1);
			}

			//error
			s = s + arma::dot(Y[k]-X[k]*mu[k],Y[k]-X[k]*mu[k]) + arma::trace(X[k]*Sigmas[k]*trans(X[k]));
		}
		//error
		errB = sbeta+s/2.0;
		err = errA/errB;

	}

	//evidence?
	double ev = 0.0;
	
	if(calc_ev==1)
	{	
		arma::colvec s = arma::zeros(K);
		for(int k=0;k<K;k++)
		{
			arma::colvec b = alpha*alpha + mu[k]%mu[k] + Sigmas[k].diag();
			double a = gamma*gamma;
			arma::colvec eiz = arma::zeros(M);
			arma::colvec ezeta = arma::zeros(M);
			arma::colvec elogz = arma::zeros(M);
			for(int m=0;m<M;m++)
			{
				double sab = sqrt(a*b(m));
				eiz[m] = pow(sqrt(b(m)/a),-1)*R::bessel_k(sab,2,1)/R::bessel_k(sab,1,1);
				ezeta[m] = sqrt(b(m)/a)*R::bessel_k(sab,0,1)/R::bessel_k(sab,-1,1);
				elogz[m] = 0.5*log(b(m)/a)-(R::bessel_k(sab,0,1)/(sab*R::bessel_k(sab,1,1)));
			}
		
			double like = (Ls[k]/2.0)*(R::digamma(errA)-log(errB))-(Ls[k]/2.0)*(log(2.0*M_PI))-(0.5*(errA/errB)*arma::dot(Y[k],Y[k])-(errA/errB)*dot(mu[k],trans(X[k])*Y[k])+0.5*(errA/errB)*dot(trans(mu[k])*trans(X[k]),X[k]*mu[k])+arma::trace(X[k]*Sigmas[k]*trans(X[k])));

			double priorBeta = arma::sum(-0.5*(log(2.0*M_PI)+elogz)-(0.5)*eiz%(mu[k]%mu[k]+Sigmas[k].diag()));


			double priorZeta = arma::sum(log(alpha/sqrt(2.0*M_PI))+alpha*gamma-1.5*elogz-0.5*((alpha*alpha)*eiz+(gamma*gamma)*ezeta));


			double val,sign;
			arma::log_det(val,sign,Sigmas[k]);
			double qBeta = (M/2.0)*(log(2.0*M_PI)+1)+0.5*val*sign;

			double qZeta = 0.0;
			for(int m=0;m<M;m++)
			{
				qZeta = qZeta - ((2.0-a*b(m))*R::bessel_k(sqrt(a*b(m)),0,1)/(sqrt(a*b(m))*R::bessel_k(sqrt(a*b(m)),1,1))-log(2.0*b(m)*R::bessel_k(sqrt(a*b(m)),1,1)/sqrt(a*b(m)))-1);
			}

			s[k] = like + priorBeta + priorZeta + qBeta + qZeta;
		}
		double priorSigma = salpha*log(sbeta)-R::lgammafn(salpha)+(salpha-1)*(R::digamma(errA)-log(errB))-sbeta*(errA/errB);
		double qSigma = errA - log(errB) + R::lgammafn(errA)+(1-errA)*R::digamma(errA);
		ev = arma::sum(s) + priorSigma + qSigma;
	}

	Rcpp::List r = Rcpp::List::create(Rcpp::Named("mu")=wrap(mu),Rcpp::Named("sigmas")=wrap(Sigmas),Rcpp::Named("sigmae")=err,Rcpp::Named("sigmaA")=errA,Rcpp::Named("sigmaB")=errB,Rcpp::Named("ev")=ev);
	return r;
}
